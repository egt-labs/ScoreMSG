﻿using System.Text;

// Following are used to extract plain text from .PDF, .DOCX and .PPTX files 
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Presentation;
using System.Configuration;


namespace ScoreMSG
{
    public class Utilities
    {
        public  void Log(string msg)
        // Logs to the console
        {
            Console.WriteLine($"{DateTime.Now:yyyy-MM-dd HH:mm:ss}: {msg}");
        }
        public  void Log(string msg, string fileName, string secondFileName = "")
        // Logs to the console and to file [fileName]
        {
            string logMessage = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss}: {msg}";
            Log(logMessage);
            using (StreamWriter writer = new StreamWriter(fileName, true))
            {
                writer.WriteLine(logMessage);
            }
            if (secondFileName  != "")
            {
                using (StreamWriter writer = new StreamWriter(secondFileName, true))
                {
                    writer.WriteLine(logMessage);
                }
            }
        }

        public string readText(string fileName)
        // reads all text from a text file
        {
            return File.ReadAllText(fileName);
        }
        public string extractWord(string fileName)
        // Extracts plain text from a .DOCX file. 
        {
            // Does not support .DOC files, which were not stored in an XML-zipped format
            Utilities u = new Utilities();
            string myText;
            try
            {
                using (WordprocessingDocument doc = WordprocessingDocument.Open(fileName, false))
                {
                    Body body = doc.MainDocumentPart.Document.Body;
                    myText=body.InnerText;
                    doc.Close();
                }
                return myText;
            }
            catch (Exception ex)
            {
                GlobalVars.u.Log("Unable to process word docment {fileName}", GlobalVars.LOG_FILE_NAME, GlobalVars.ERROR_FILE_NAME);
                GlobalVars.u.Log(ex.Message, GlobalVars.LOG_FILE_NAME, GlobalVars.ERROR_FILE_NAME);
                return "";
            }
        }
        public string extractPDF(string fileName)
        // Extracts plain text from a .PDF file. 
        {
            Utilities u = new Utilities();
            try
            {
                StringBuilder text = new StringBuilder();
                using (PdfReader reader = new PdfReader(fileName))
                {
                    for (int page = 1; page <= reader.NumberOfPages; page++)
                    {
                        text.Append(PdfTextExtractor.GetTextFromPage(reader, page));
                    }
                    reader.Close();
                }
                
                return text.ToString();
            }
            catch (Exception ex) 
            {
                GlobalVars.u.Log("Unable to process PDF docment {fileName}", GlobalVars.LOG_FILE_NAME, GlobalVars.ERROR_FILE_NAME);
                GlobalVars.u.Log(ex.Message, GlobalVars.LOG_FILE_NAME);
                return "";
            }
        }
        public string extractPPT(string fileName)
        // Extracts plain text from a .PPTX file. 
        {
            // Does not support .PPT files which do not use the XML-zipped format
            Utilities u = new Utilities();
            string myText;
            try
            {
                using (PresentationDocument presentationDocument = PresentationDocument.Open(fileName, false))
                {
                    SlidePart[] slideParts = presentationDocument.PresentationPart.SlideParts.ToArray();
                    StringBuilder stringBuilder = new StringBuilder();
                    foreach (SlidePart slidePart in slideParts)
                    {
                        Slide slide = slidePart.Slide;
                        TextBody textBody = slide.Descendants<TextBody>().FirstOrDefault();
                        if (textBody != null)
                        {
                            stringBuilder.Append(textBody.InnerText);
                        }
                    }
                    myText= stringBuilder.ToString();
                    return myText;
                }

            }
            catch (Exception ex)
            {
                GlobalVars.u.Log("Unable to process PPT docment {fileName}", GlobalVars.LOG_FILE_NAME, GlobalVars.ERROR_FILE_NAME);
                GlobalVars.u.Log(ex.Message, GlobalVars.LOG_FILE_NAME);
                return "";
            }
        }
        public string extractText(string fileName)
        // Determines how to extract plain text from a file based on the file extension
        {
            Utilities u = new Utilities();
            string extension = System.IO.Path.GetExtension(fileName).ToUpper();
            GlobalVars.u.Log($"Extracting text from file {fileName}...", GlobalVars.LOG_FILE_NAME);
            switch (extension)
            {
                case ".TXT":
                    return readText(fileName);
                case ".DOCX":
                    return extractWord(fileName);
                case ".PDF":
                    return extractPDF(fileName);
                case ".PPTX":
                    return extractPPT(fileName);
                case ".MSG":
                    // we don't want the default "unsupported..." error message for .MSG files
                    return "";
                default:
                    GlobalVars.u.Log($"Unsupported file type for plain text extraction: {extension} in document {System.IO.Path.GetFileName(fileName)}", GlobalVars.LOG_FILE_NAME, GlobalVars.ERROR_FILE_NAME);
                    break;
            }
            return "";
        }
        public long countWords(string searchString)
        {
            int n = 0;
            long numWords = 1;

            /* loop till end of string */
            while (n <= searchString.Length - 1)
            {
                /* check whether the current character is white space or new line or tab character*/
                if (searchString[n] == ' ' || searchString[n] == '\n' || searchString[n] == '\t')
                {
                    numWords++;
                }
                n++;
            }
            return numWords;
        }
        public void checkParams(bool exitYN, string happy, string sad)
        // Checks a boolean statement and outputs a message, cancelling the program if the statement is false
        {
            Console.WriteLine(exitYN == true ? happy : sad);
        }
        public string getConfig(string key, string valueDefault)
        {
            string keyValue = ConfigurationManager.AppSettings.Get(key);
            return String.IsNullOrEmpty(keyValue)  ? valueDefault : keyValue;
        }
    }
}
