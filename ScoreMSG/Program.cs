﻿using System.Diagnostics;
using System.IO.Compression;
using static ScoreMSG.Classes;

namespace ScoreMSG
{
    static class GlobalVars
    {
        // All files will be saved (for convenience) in the same folder as the .EXE
        //Configurable variables stored in app.config
        public static Utilities u = new Utilities();

        // Read configurable values from app.config
        // Entries in app.config are optional and will override the values in the second parameter of each of the below assignments
        public static string configLogFileName= u.getConfig("LogFileName", "ScoreMSG.log");
        public static string errorLogFileName = u.getConfig("ErrorFileName", "Errors.log");
        public static string configAgencyFileName = u.getConfig("AgencyFileName", "agencies.txt");
        public static string configCapabilityFileName = u.getConfig("CapabilityFileName", "capabilities.txt");
        public static int configCapabilityTotalThreshhold = Int32.Parse(u.getConfig("CapabilityTotalThreshhold", "10"));
        public static int configAgencyTotalThreshhold = Int32.Parse(u.getConfig("AgencyTotalThreshhold", "10"));
        public static int configCombinedTotalThreshhold = Int32.Parse(u.getConfig("CombinedTotalThreshhold", "50"));
        public static int configKeywordToAttachmentThreshhold = Int32.Parse(u.getConfig("KeywordToAttachmentThreshhold", "25"));
        public static int configNoAttachmentMinimumAgencyCapabilityThreshhold = Int32.Parse(u.getConfig("NoAttachmentMinimumAgencyCapabilityThreshhold", "0"));
        public static string configStartupEmailNotification = u.getConfig("StartupEmail", "");


        public static string LOG_FILE_NAME = null; 
        public static string ERROR_FILE_NAME = null;
        // The folder to watch for new .MSG files
        public static string WATCH_FOLDER = ""; // = null;
        // These two files contain the search keywords for agencies and capabilities. Each search term 
        // lives on its own line in the file.
        public static string AGENCIES_KEYWORD_FILE_NAME = null;
        public static string CAPABILITIES_KEYWORD_FILE_NAME = null;
        // The folder containing the above two keywords files
        public static string KEYWORDS_FOLDER = null;
        // Email address of the person notified of HIGH priority IDIQ messages
        public static string NOTIFICATION_RECIPIENT = null;
        // Email address of the person notified of when the system starts (used to load Outlook)
        public static string STARTUP_RECIPIENT = null;
        // Hold the individual keywords
        public static Classes.keywordFile agencies = null;
        public static Classes.keywordFile capabilities = null;
        
        public static string watchingMessage = "Watching...\r\nPress Ctrl-Break to exit.";
        // How often to wait between polling the watch folder
        public static byte timerPollingSeconds = 5;
    }
    class Program
    {
        static void Main(string[] args)
        {
            #region Validate command line parameters

            GlobalVars.u.checkParams(args.Length == 3, "Found three command line parameters", $"Did not find three command line parameters ({args.Length}).\r\nSyntax: scoremsg.exe [watch folder] [keywords folder] [notification email address]\r\nExample: scoremsg.exe c:\\temp c:\\temp\\keywords terrence.blair@tetratech.com");
            GlobalVars.WATCH_FOLDER = args[0];
            GlobalVars.KEYWORDS_FOLDER = args[1];
            GlobalVars.u.checkParams(Path.Exists(GlobalVars.WATCH_FOLDER), $"Found watch folder {GlobalVars.WATCH_FOLDER}", $"Watch folder {GlobalVars.WATCH_FOLDER} does not exist");
            GlobalVars.AGENCIES_KEYWORD_FILE_NAME= Path.Combine(GlobalVars.KEYWORDS_FOLDER, GlobalVars.configAgencyFileName);
            GlobalVars.CAPABILITIES_KEYWORD_FILE_NAME= Path.Combine(GlobalVars.KEYWORDS_FOLDER, GlobalVars.configCapabilityFileName);
            GlobalVars.u.checkParams(((Path.Exists(GlobalVars.AGENCIES_KEYWORD_FILE_NAME))) && (Path.Exists(GlobalVars.CAPABILITIES_KEYWORD_FILE_NAME)), $"Found {GlobalVars.configAgencyFileName} and {GlobalVars.configCapabilityFileName} keyword files", $"{GlobalVars.configAgencyFileName} and {GlobalVars.configCapabilityFileName} keyword files missing from folder {GlobalVars.KEYWORDS_FOLDER}");
            GlobalVars.NOTIFICATION_RECIPIENT= args[2];
            GlobalVars.STARTUP_RECIPIENT = GlobalVars.configStartupEmailNotification;
            GlobalVars.LOG_FILE_NAME = Path.Combine(Directory.GetCurrentDirectory(), GlobalVars.configLogFileName);
            GlobalVars.ERROR_FILE_NAME = Path.Combine(Directory.GetCurrentDirectory(), GlobalVars.errorLogFileName);
            Console.WriteLine($"Sending notifications to {GlobalVars.NOTIFICATION_RECIPIENT}");
            Console.WriteLine($"Command line paramters validated.");
            Console.WriteLine($"Logging to file {GlobalVars.LOG_FILE_NAME}...");
            GlobalVars.u.Log($"==================", GlobalVars.LOG_FILE_NAME);
            GlobalVars.u.Log($"SESSION STARTED", GlobalVars.LOG_FILE_NAME);
            GlobalVars.u.Log($"==================", GlobalVars.LOG_FILE_NAME);
            #endregion

            if (GlobalVars.STARTUP_RECIPIENT != "")
            {
                sendStartupEmail();
            }

            //#region Watch a folder, looking for files ending in .MSG.
            //FileSystemWatcher watcher = new FileSystemWatcher();
            //watcher.Path = GlobalVars.WATCH_FOLDER;
            //watcher.Filter = "*.msg"; // Filter for .msg files
            //watcher.EnableRaisingEvents = true;
            //watcher.Created += OnCreated; // Event handler for new files created
            //#endregion

            //#region Watch a folder, looking changes to the keyword files
            //FileSystemWatcher keywordWatcher = new FileSystemWatcher();
            //keywordWatcher.Path = GlobalVars.KEYWORDS_FOLDER;
            //keywordWatcher.Filter = "*.txt"; // Filter for .txt files - all keyword files end in .TXT
            //keywordWatcher.EnableRaisingEvents = true;
            //keywordWatcher.Changed += OnChanged; // Event handler for new files created
            //#endregion

            loadKeywords();

            string[] myMSGFiles;
            DateTime agenciesLastChanged = File.GetLastWriteTime(GlobalVars.AGENCIES_KEYWORD_FILE_NAME);
            DateTime capabilitiesLastChanged = File.GetLastWriteTime(GlobalVars.CAPABILITIES_KEYWORD_FILE_NAME);
            while (true)
            {
                // Poll the watch folder for files ending in .MSG
                myMSGFiles = System.IO.Directory.GetFiles(GlobalVars.WATCH_FOLDER, "*.msg", System.IO.SearchOption.TopDirectoryOnly);
                if (myMSGFiles.Length > 0)
                {
                    OnCreated();
                }
                // Poll the keyword folder for changes to the metdata on the agencies.txt and capabilities.txt files
                if ((File.GetLastWriteTime(GlobalVars.AGENCIES_KEYWORD_FILE_NAME) != agenciesLastChanged) || (File.GetLastWriteTime(GlobalVars.CAPABILITIES_KEYWORD_FILE_NAME) != capabilitiesLastChanged))
                {
                    agenciesLastChanged = File.GetLastWriteTime(GlobalVars.AGENCIES_KEYWORD_FILE_NAME);
                    capabilitiesLastChanged = File.GetLastWriteTime(GlobalVars.CAPABILITIES_KEYWORD_FILE_NAME);
                    loadKeywords();
                }
                System.Threading.Thread.Sleep(GlobalVars.timerPollingSeconds * 1000);
            }
        }
        private static void loadKeywords()
        {
            // Load keyords as global value; Changes in keywords in the .TXT files will be caught by the FileWatcher and these values will be reloaded
            GlobalVars.agencies = new Classes.keywordFile(GlobalVars.AGENCIES_KEYWORD_FILE_NAME);
            GlobalVars.capabilities = new Classes.keywordFile(GlobalVars.CAPABILITIES_KEYWORD_FILE_NAME);
            GlobalVars.u.Log($"Loaded keywords", GlobalVars.LOG_FILE_NAME);
            Console.WriteLine(GlobalVars.watchingMessage);
        }
        private static void OnCreated()
        {
            // OnCreated fires when a *new* .MSG file is placed into the watch directory.
            // Overwriting an existing file will not fire OnCreated
            GlobalVars.u.Log("===========================================", GlobalVars.LOG_FILE_NAME);
            GlobalVars.u.Log($"New file(s) detected in folder {GlobalVars.WATCH_FOLDER}", GlobalVars.LOG_FILE_NAME);
            GlobalVars.u.Log("===========================================", GlobalVars.LOG_FILE_NAME);

            // The while loop is used to process all .MSG files that it finds
            // whenever a single file is found to make sure that no .MSG files are missed
            string[] filesToProcess = System.IO.Directory.GetFiles(GlobalVars.WATCH_FOLDER, "*.msg", System.IO.SearchOption.TopDirectoryOnly);
            string myFileName;
            string myFilePath;
            while (filesToProcess.Length > 0)
            {
                // Used to log how long it takes to process a .MSG file
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                myFileName = Path.GetFileName(filesToProcess[0]);
                myFilePath = Path.GetDirectoryName(filesToProcess[0]);
                GlobalVars.u.Log("===========================================", GlobalVars.LOG_FILE_NAME);
                GlobalVars.u.Log($"Processing file : {myFileName}", GlobalVars.LOG_FILE_NAME);
                GlobalVars.u.Log("===========================================", GlobalVars.LOG_FILE_NAME);
                processMSGFile(filesToProcess[0]);
                stopwatch.Stop();
                GlobalVars.u.Log($"Message processed in {stopwatch.ElapsedMilliseconds / 1000} seconds ({stopwatch.ElapsedMilliseconds} milliseconds).", GlobalVars.LOG_FILE_NAME);
                
                filesToProcess = System.IO.Directory.GetFiles(GlobalVars.WATCH_FOLDER, "*.msg", System.IO.SearchOption.TopDirectoryOnly);
            }
            Console.WriteLine(GlobalVars.watchingMessage);
        }
        private static void sendStartupEmail()
        {
            Classes.NotificationEmail e = new Classes.NotificationEmail();
            e.subject = "Startup Email Notification";
            e.recipient = GlobalVars.STARTUP_RECIPIENT;
            e.sendNotification();
        }
        private static void processMSGFile(string fileName)
        {
            #region Load email .MSG file
            Classes.MSGFile myMSGFile = new Classes.MSGFile(fileName);
            #endregion
            // myFilesToSearch will contain all of the file objects which we will search for keywords
            List<Classes.fileToSearch> myFilesToSearch = new List<Classes.fileToSearch>();
            // myZipFilesToSearch will temporarily contain the list of any files in a .ZIP attachment; they will be added to myFilesToSearch
            List<Classes.fileToSearch> myZipFilesToSearch = new List<Classes.fileToSearch>();
            #region Process each attachment in the email
            foreach (string attFileName in myMSGFile.attachments)
            {
                // Creating a new class with fileToSearch will create an object with metadata about the file and
                // the plain text contents of the file 
                // Class fileToSearch does most of the heavy lifting
                myFilesToSearch.Add(new Classes.fileToSearch(attFileName));
            }
            #endregion
            string newFileName;
            int tempBound = myFilesToSearch.Count;
            #region Process each attachment in the .MSG file and determine if any of them are .ZIP files that need to be extracted
            for (int i = 0; i < tempBound; i++)
            {
                if (myFilesToSearch[i].extension.ToUpper() == ".ZIP")
                //.ZIP files are a PITA - the below does recurse nested folders in a .ZIP but does not unzip embedded .ZIPs in a .ZIP file
                {
                    using (ZipArchive zipArchive = ZipFile.OpenRead(myFilesToSearch[i].fullName))
                    {
                        ZipArchiveEntry[] entries = zipArchive.Entries.ToArray();
                        foreach (ZipArchiveEntry entry in entries)
                        {
                            // Extract any files in the attached .ZIP file to the file system 
                            newFileName = Path.Combine(myFilesToSearch[i].folder, entry.FullName);
                            newFileName = newFileName.Replace("/", "__");
                            entry.ExtractToFile(newFileName, overwrite: true);
                            // add the newly extracted files to the master list of files to search
                            myFilesToSearch.Add(new Classes.fileToSearch(newFileName));
                        }
                    }
                }
            }
            #endregion

            // Now that we've extracted the searchable information from each file attachment,
            // we don't need them anymore
            foreach (Classes.fileToSearch f in myFilesToSearch)
            {
                f.deleteFile();
                GlobalVars.u.Log($"File {f.fullName} was deleted from the filesystem.", GlobalVars.LOG_FILE_NAME);
            }
            // Eliminate files from the myFilesToSearch list that don't contain any content that can be searched 
            // for keywords - this is usually due to unsupported file formats (e.g., .JPG, .DOC files)
            myFilesToSearch.RemoveAll(x => x.rawtext == "");

            int agencyTotal = 0;
            int capabilityTotal = 0;

            #region Perform frequency keyword search result counts against all .MSG and attachment files and save the count to the fileToSearch object
            // Display keywords counts found in the .MSG file
            GlobalVars.u.Log($"{myMSGFile.agencyScore} agency keywords were found in file {myMSGFile.name}", GlobalVars.LOG_FILE_NAME);
            GlobalVars.u.Log($"{myMSGFile.capabilityScore} capability keywords were found in file {myMSGFile.name}", GlobalVars.LOG_FILE_NAME);
            agencyTotal = agencyTotal + myMSGFile.agencyScore;
            capabilityTotal = capabilityTotal + myMSGFile.agencyScore;
            
            long totalFileWords = 0;

            // Display keywords counts found in each attachment
            foreach (Classes.fileToSearch f in myFilesToSearch)
            {
                totalFileWords = GlobalVars.u.countWords(f.rawtext);
                GlobalVars.u.Log($"{f.agencyScore} agency keywords were found in file {f.name}", GlobalVars.LOG_FILE_NAME);
                GlobalVars.u.Log($"{f.capabilityScore} capability keywords were found in file {f.name}", GlobalVars.LOG_FILE_NAME);
                agencyTotal = agencyTotal + f.agencyScore;
                capabilityTotal = capabilityTotal + f.capabilityScore;
            }
            #endregion
            #region Calculate what warrants an email
            bool isHigh = ((capabilityTotal >= GlobalVars.configCapabilityTotalThreshhold && agencyTotal >= GlobalVars.configAgencyTotalThreshhold) && (capabilityTotal + agencyTotal >= GlobalVars.configCombinedTotalThreshhold));
            if (!isHigh) { isHigh = ((myFilesToSearch.Count == 0) && (agencyTotal >= GlobalVars.configNoAttachmentMinimumAgencyCapabilityThreshhold) && (capabilityTotal >= GlobalVars.configNoAttachmentMinimumAgencyCapabilityThreshhold)); }
            if (!isHigh) { isHigh = ((myFilesToSearch.Count > 0) && (agencyTotal+capabilityTotal > 0) && ((agencyTotal + capabilityTotal) / 10 >= GlobalVars.configKeywordToAttachmentThreshhold)); }

            if (isHigh)
            {
                GlobalVars.u.Log($"Sending email to {GlobalVars.NOTIFICATION_RECIPIENT} with HIGH priority IDIQ.", GlobalVars.LOG_FILE_NAME);
                Classes.NotificationEmail e = new Classes.NotificationEmail();
                e.theMessage = myMSGFile;
                e.recipient = GlobalVars.NOTIFICATION_RECIPIENT;
                e.capabilityTotal = capabilityTotal;
                e.agencyTotal = agencyTotal;
                e.attachmentsToSearch = myFilesToSearch;
                e.sendNotification();
                GlobalVars.u.Log("Email Text:",GlobalVars.LOG_FILE_NAME);
                GlobalVars.u.Log(e.emailBody, GlobalVars.LOG_FILE_NAME);
            }
            else
            {
                GlobalVars.u.Log(".MSG file was not HIGH priority. Not sending email.", GlobalVars.LOG_FILE_NAME);
            }
            #endregion
            // We no longer need the .MSG file in the file system since it has been forwarded 
            myMSGFile.deleteFile();
            GlobalVars.u.Log($"File {myMSGFile.fullName} was deleted from the filesystem.", GlobalVars.LOG_FILE_NAME);
        }
    }
}
