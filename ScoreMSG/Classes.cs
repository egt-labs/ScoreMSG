﻿using MsgReader.Outlook;
using System.Text.RegularExpressions;

// Note that this program must run on a computer which
// has Microsoft Outlook installed.  It uses Microsoft 
// Outlook to send the notification emails as the 
// logged-in user in order to avoid dealing with 
// MFA authentication hassles connecting to the Tetra
// Tech O365 email server directly via SMTP.  (Tetra Tech
// also doesn't seem to have "Authenticated SMTP" enabled in their tenant.)
using Microsoft.Office.Interop.Outlook;


namespace ScoreMSG
{
    internal class Classes
    {
        public class fileToSearch
        {
            // fileToSearch contains all of the metadata about each file to be processed for search terms

            // metadata about the file in the file system
            public string extension = "";
            public string fullName = "";
            public string name = "";
            public string folder = "";
            // contains the plain text content from the file
            public string rawtext = "";

            // contains the frequency of each search term for a file
            public int agencyScore=0;
            public int capabilityScore=0;

            ScoreMSG.Utilities u = new ScoreMSG.Utilities();

            public fileToSearch(string fileName, bool suppressKeywords=false )
            {
                if (File.Exists(fileName))
                {
                    fullName = fileName;
                    folder = Path.GetDirectoryName(fileName);
                    extension = Path.GetExtension(fileName).ToUpper();
                    name = Path.GetFileName(fileName);
                    // fill the rawtext attribute by processing the file based on file type
                    rawtext = GlobalVars.u.extractText(fileName);
                    if (!suppressKeywords)
                    {
                        getKeywordCounts();
                    }
                }
            }
            public void getKeywordCounts()
            {
                long totalWords = 0;
                if (rawtext != "")
                {
                    totalWords=GlobalVars.u.countWords(rawtext);
                    // Search agency keywords against the contents of the rawtext field
                    foreach (string s in GlobalVars.agencies.searchKeywords)
                    {
                        if (s.Length>2)
                        { 
                        agencyScore = agencyScore + Regex.Matches(rawtext.ToUpper(), s.ToUpper()).Count;
                        }
                    }
                    // Search capability keywords the contents of the rawtext field
                    totalWords = GlobalVars.u.countWords(rawtext);
                    foreach (string s in GlobalVars.capabilities.searchKeywords)
                    {
                        if (s.Length > 2)
                        {
                            capabilityScore = capabilityScore + Regex.Matches(rawtext.ToUpper(), s.ToUpper()).Count;
                        }
                    }
                }
            }
            public bool deleteFile()
            {
                // Deletes a file.  Retry process required due to potential use of OneDrive files as a 
                // file share.  OneDrive has some save latency so on occasion files are not fully saved
                // before the program tries to delete them.  
                // In my testing, no more than one retry is required, but it will try for up to five seconds
                byte retries = 0;
                byte maxRetries = 5;
                while (true)
                {
                    try
                    {
                        File.Delete(fullName);
                        break;
                    }
                    catch //(System.Exception e)
                    {
                        if (retries > 5)
                        {
                            GlobalVars.u.Log($"Unable to delete file {fullName} after {maxRetries} retries.  Exited program.", GlobalVars.LOG_FILE_NAME, GlobalVars.ERROR_FILE_NAME);
                            return false;
                        }
                        retries++;
                        GlobalVars.u.Log($"Unable to delete file {fullName}. Retry #{retries} of {maxRetries}...", GlobalVars.LOG_FILE_NAME);
                        System.Threading.Tasks.Task.Delay(retries * 1000).Wait();
                    }
                }
                return true;
            }
        } 
        public class MSGFile : fileToSearch
        {
            // Inherits from class fileToSearch, but adds some attributes specific to MSG files (e.g., attachments)
            Utilities u = new Utilities();
            public List<string> attachments = new List<string>();
            //public string sender = "";
            public MSGFile(string fileName) : base(fileName)
            {
                try
                {
                    // some (very, very few) .MSG files can be malformed enough that
                    // they are openable, but not closeable, by MSGReader.
                    // To test for this, we will make a copy of the .MSG file to a temp 
                    // file, attempt to open it and then delete it.  If it fails to be
                    // openable or deleteable, then we will NOT process the original
                    // .MSG file, we will just delete it.
                    // This appears to be an undocumented issue with MSGReader
                    string tmpFileName = Path.GetTempFileName();
                    File.Copy(fileName, tmpFileName,true);
                    var msgTemp = new MsgReader.Outlook.Storage.Message(tmpFileName);
                    msgTemp.Dispose();
                    File.Delete(tmpFileName);
                    GlobalVars.u.Log($"File {fileName} structure is valid");

                    using (var msg = new MsgReader.Outlook.Storage.Message(fullName))
                    {
                        // the search text for the .MSG file is only the subject and the body.
                        // its attachments are separately processed as their own objects, with their
                        // own storage of content
                        rawtext = msg.BodyText + Environment.NewLine + msg.Subject;
                        //sender = msg.Sender.ToString();
                        foreach (var thisAttachment in msg.Attachments)
                        {
                            // Process all attachments in the .MSG file
                            if (thisAttachment is Storage.Attachment attachment)
                            {
                                // Save the attachment to the file system for processing
                                string saveFileName = Path.Combine(folder, attachment.FileName);
                                System.IO.File.WriteAllBytes(saveFileName, attachment.Data);
                                attachments.Add(saveFileName);
                            }
                        }
                        msg.Dispose();
                    }
                }
                catch //(System.Exception e) 
                {
                    // Note that file [tmpFileName] must be manually deleted
                    GlobalVars.u.Log($"ERROR: File {fileName} could not be processed by MSGReader and/or was an invalid .MSG file. Deleted without processing", GlobalVars.LOG_FILE_NAME, GlobalVars.ERROR_FILE_NAME);
                    File.Delete(fileName);
                }
            }
        }
        public class keywordFile : fileToSearch
        {
            // Inherits from class fileToSearch, but adds some attributes specific to keyword files (e.g., keyword search terms)
            public List<string> searchKeywords = new List<string>();
            private string[] lines;
            public keywordFile(string fileName) : base(fileName, true)
            {
                // read the keyword file and save each line as a 
                // separate search term into the searchKeywords property
                Utilities u= new Utilities();
                lines = rawtext.Split(Environment.NewLine);
                foreach (string kword in lines)
                {
                    searchKeywords.Add (kword);
                }
            }
        }
        public class NotificationEmail
        {
            public fileToSearch theMessage;
            public List<Classes.fileToSearch> attachmentsToSearch;
            public string recipient = "";  //default value, overwridden at command line
            public int capabilityTotal = 0;
            public int agencyTotal = 0;
            public string emailBody = "";
            public string subject = "HIGH PRIORITY IDIQ";
            public  void sendNotification()
            {
                constructEmail();
                sendNotificationEmail();
            }
            private void constructEmail()
            {
                // Construct the body of the notification email 
                emailBody = $"Hello,{Environment.NewLine}\r\n";
                emailBody = emailBody + $"We believe the attached IDIQ opportunity has a HIGH likelihood of being interesting to FIT business development staff.\r\n";
                emailBody = emailBody + $"Across the attached email ";
                if (attachmentsToSearch != null)
                {
                    if (attachmentsToSearch.Count > 0)
                    {
                        emailBody = emailBody + $"and all of its {attachmentsToSearch.Count} attachments, ";
                    }
                }
                emailBody = emailBody + $"we found mention of your capability keywords ({capabilityTotal}) times and your agency keywords ({agencyTotal}) times.\r\n";
                emailBody = emailBody + "---------------------------------------\r\n";
                if (theMessage != null)
                {
                    if (theMessage.capabilityScore + theMessage.agencyScore > 2)
                    {
                        emailBody = emailBody + $"- The content of the email body found mention of your capability keywords ({theMessage.capabilityScore}) times and your agency keywords ({theMessage.agencyScore}) times.\r\n";
                    }
                }
                if (attachmentsToSearch != null)
                {
                    if (attachmentsToSearch.Count > 0)
                    {
                        foreach (Classes.fileToSearch f in attachmentsToSearch)
                        {
                            if (f.capabilityScore + f.agencyScore > 3)
                            {
                                emailBody = emailBody + $"- Attachment {f.name} mentioned your capability keywords ({f.capabilityScore}) times and your agency keywords ({f.agencyScore}) times.\r\n";
                            }
                        }
                    }
                }
            }

            private void sendNotificationEmail()
            {
                // Send the email
                Application outlookApp = new Microsoft.Office.Interop.Outlook.Application();
                MailItem mail = outlookApp.CreateItem(OlItemType.olMailItem) as MailItem;
                mail.To = recipient;
                mail.Subject = subject;
                mail.Body = emailBody;
                if (theMessage != null)
                {
                    Attachment outlookAttachment = mail.Attachments.Add(theMessage.fullName);
                }
                mail.Send();
                //outlookApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(outlookApp);
            }

        }
    }
}
